﻿using System;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

[Serializable]
public struct SourceImageAndName
{
    public Image SourceImage;
    public string ImageName;
}
public class SpritesManager : MonoBehaviour
{
    private string spriteName;
    [SerializeField] private SpriteAtlas _atlas;
    public SourceImageAndName[] sourceImages;
    
    void Start()
    {
        spriteAssign();
    }

    private void spriteAssign()
    {
        var length = sourceImages.Length;
        for (int i = 0; i < length; i++)
        {
            spriteName = sourceImages[i].ImageName;
            sourceImages[i].SourceImage.sprite = _atlas.GetSprite(spriteName);
        }
    }
}
